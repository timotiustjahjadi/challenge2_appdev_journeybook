//
//  ExplorerCollectionViewCell.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 11/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class ExplorerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ExplorerImageView: UIImageView!
    @IBOutlet weak var ExplorerNameLabel: UILabel!
    @IBOutlet weak var ExplorerRoleLabel: UILabel!
    
    func setExplorer(senExplorer: SenExplorer){
        ExplorerImageView.image = UIImage(named: senExplorer.SeniorExplorerImage)
        ExplorerNameLabel.text = senExplorer.SeniorExplorerName
        ExplorerRoleLabel.text = senExplorer.SeniorExplorerRole.uppercased()
    }
}
