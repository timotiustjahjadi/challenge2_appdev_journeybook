//
//  JourneyListViewController.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 10/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

var dataAppearAll:[Int] = [0, 1, 2, 3, 4, 5]
var dataAppearGroup:[Int] = [0, 2, 3, 5]
var dataAppearIndividu:[Int] = [1, 4]
var rowtoDisplay = dataAppearAll
var category:Int = 0

class JourneyListViewController: UIViewController {

    @IBOutlet weak var CategorySegmented: UISegmentedControl!
    
    @IBAction func CategoryChangec(_ sender: Any) {
        switch CategorySegmented.selectedSegmentIndex
        {
        case 0:
            category = 0
            rowtoDisplay = dataAppearAll
        case 1:
            category = 1
            rowtoDisplay = dataAppearGroup
        case 2:
            category = 2
            rowtoDisplay = dataAppearIndividu
        default:
            break
        }
        
        JourneyTableView.reloadData()
    }
    
    @IBOutlet weak var JourneyTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        JourneyTableView.delegate = self
        JourneyTableView.dataSource = self
        JourneyTableView.separatorColor = UIColor(white: 0, alpha: 0)
        
    }
    
    var arrayOfJourney:[Journey] = Journey.fetchDataJourney()
    
    var JourneyIndex:Int = 0
    
}

extension JourneyListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowtoDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let fill:Int = dataAppearGroup[0]
//        print(dataAppearGroup)
        let journey = arrayOfJourney[rowtoDisplay[indexPath.row]]
//        if dataAppearGroup.count > 1 {
//            dataAppearGroup.removeFirst()
//        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JourneyCell") as? JourneyTableViewCell
        
        cell?.setJourneys(journey: journey)
        
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch category
        {
        case 0:
            let vc = storyboard?.instantiateViewController(identifier: "coba") as? JourneyDetailViewController
            vc?.data = arrayOfJourney[indexPath.row]
            self.navigationController?.pushViewController(vc!, animated: true)
        case 1:
            if indexPath.row == 0 {
                let vc = storyboard?.instantiateViewController(identifier: "coba") as? JourneyDetailViewController
                vc?.data = arrayOfJourney[0]
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 1 {
                let vc = storyboard?.instantiateViewController(identifier: "coba") as? JourneyDetailViewController
                vc?.data = arrayOfJourney[2]
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 2 {
                let vc = storyboard?.instantiateViewController(identifier: "coba") as? JourneyDetailViewController
                vc?.data = arrayOfJourney[3]
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 3 {
                let vc = storyboard?.instantiateViewController(identifier: "coba") as? JourneyDetailViewController
                vc?.data = arrayOfJourney[5]
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            
        case 2:
            if indexPath.row == 0 {
                let vc = storyboard?.instantiateViewController(identifier: "coba") as? JourneyDetailViewController
                vc?.data = arrayOfJourney[1]
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if indexPath.row == 1 {
                let vc = storyboard?.instantiateViewController(identifier: "coba") as? JourneyDetailViewController
                vc?.data = arrayOfJourney[4]
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            
        default:
            break
        }
        
        
    }

}
