//
//  SeniorExplorer.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 11/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import Foundation
//import UIKit

struct SenExplorer {
    var SeniorExplorerImage:String
    var SeniorExplorerName:String
    var SeniorExplorerRole:String
    var SeniorCanHelp:String
    var SeniorNeedHelp:String
    var SeniorPhone:String
    var SeniorEmail:String
    
    static func fetchSeniorData() -> [SenExplorer]{
    var arrayOfSeniorExp: [SenExplorer]
        
        arrayOfSeniorExp = [
            SenExplorer(SeniorExplorerImage: "Ari_SE", SeniorExplorerName: "Ari Kurniawan", SeniorExplorerRole: "Design Facilitator", SeniorCanHelp: "Graphic Design (layout/composition, typography, branding), HIG, UI/UX", SeniorNeedHelp: "Basic Coding", SeniorPhone: "081267853958", SeniorEmail: "akurniawan@ciputra.ac.id"),
            SenExplorer(SeniorExplorerImage: "Bayu_SE", SeniorExplorerName: "Bayu Prasetya", SeniorExplorerRole: "Design Facilitator ", SeniorCanHelp: "Visual Branding, Design Thinking, Development Process, UI/UX, Nemenin Mikir, Makan-makan (soon)", SeniorNeedHelp: "", SeniorPhone: "081278795748", SeniorEmail: "bprasetya@ciputra.ac.id"),
            SenExplorer(SeniorExplorerImage: "Dickson_SE", SeniorExplorerName: "Dickson Leonard", SeniorExplorerRole: "Academy Manager", SeniorCanHelp: "Coding basics (algorithm, data structure, operations), App development process (problem definition, ideation, prototyping, production), Basic UX, Professional skills (leadership, communication)", SeniorNeedHelp: "", SeniorPhone: "082154573846", SeniorEmail: "dleonard@ciputra.ac.id"),
            SenExplorer(SeniorExplorerImage: "Fanny_SE", SeniorExplorerName: "Fanny Halim", SeniorExplorerRole: "Coding Facilitator", SeniorCanHelp: "Basic iOS Development (Swift, XCode, Auto Layout, Data Collection), Discussion", SeniorNeedHelp: "English Speaking", SeniorPhone: "081267853958", SeniorEmail: "akurniawan@ciputra.ac.id"),
            SenExplorer(SeniorExplorerImage: "Gabriele_SE", SeniorExplorerName: "Gabriele Wijasa", SeniorExplorerRole: "Design Facilitator", SeniorCanHelp: "Graphic Design, UI/UX, Photography", SeniorNeedHelp: "", SeniorPhone: "081267853958", SeniorEmail: "akurniawan@ciputra.ac.id"),
            SenExplorer(SeniorExplorerImage: "Januar_SE", SeniorExplorerName: "Januar Tanzil", SeniorExplorerRole: "Coding Facilitator", SeniorCanHelp: "Swift language, IOS Development, Game Development, Software Engineering in general", SeniorNeedHelp: "", SeniorPhone: "081267853958", SeniorEmail: "akurniawan@ciputra.ac.id"),
            SenExplorer(SeniorExplorerImage: "Jaya_SE", SeniorExplorerName: "Jaya Pranata", SeniorExplorerRole: "Coding Facilitator", SeniorCanHelp: "Coding basic, Swift Language, Auto Layout, Core Data, Map", SeniorNeedHelp: "Graphic Design (layout/composition, typography, branding), UI/UX, problem analysis, basic research, HIG", SeniorPhone: "081267853958", SeniorEmail: "akurniawan@ciputra.ac.id"),
            SenExplorer(SeniorExplorerImage: "Ketaren_SE", SeniorExplorerName: "John Alan Ketaren", SeniorExplorerRole: "Professional Facilitator", SeniorCanHelp: "Ideation, bringing resources, user testing, business, fintech, Presentation,Networking.", SeniorNeedHelp: "Basic Xcode", SeniorPhone: "081267853958", SeniorEmail: "akurniawan@ciputra.ac.id"),
            SenExplorer(SeniorExplorerImage: "Kukuh_SE", SeniorExplorerName: "Rachmat Kukuh Rahadiansyah", SeniorExplorerRole: "Coding Facilitator", SeniorCanHelp: "General iOS app development, iOS frameworks, Swift language", SeniorNeedHelp: "", SeniorPhone: "081267853958", SeniorEmail: "akurniawan@ciputra.ac.id"),
            SenExplorer(SeniorExplorerImage: "Ryan_SE", SeniorExplorerName: "Yehezkiel Cheryan Tjandra", SeniorExplorerRole: "Professional Facilitator", SeniorCanHelp: "English language, Public speaking, Business presentation, Business writing, Business insights", SeniorNeedHelp: "Swift/Xcode in general", SeniorPhone: "081267853958", SeniorEmail: "akurniawan@ciputra.ac.id"),
            SenExplorer(SeniorExplorerImage: "Yulibar_SE", SeniorExplorerName: "Yulibar Husni", SeniorExplorerRole: "Coding Facilitator", SeniorCanHelp: "Swift, Ideation, Animation production", SeniorNeedHelp: "Ideation, English Speaking, Project Management", SeniorPhone: "081267853958", SeniorEmail: "akurniawan@ciputra.ac.id")
        ]
        return arrayOfSeniorExp
    }
}
