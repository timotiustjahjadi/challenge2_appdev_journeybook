//
//  JunExpListViewController.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 12/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit


class JunExpListViewController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet weak var SearchBarExp: UISearchBar!
    
    var filteredName:[JunExplorer] = []
   
    @IBOutlet weak var JunExpListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SearchBarExp.delegate = self
        filteredName = arrayOfJuniorExp
        JunExpListTableView.delegate = self
        JunExpListTableView.dataSource = self
    }

    let arrayOfJuniorExp:[JunExplorer] = JunExplorer.fetchJuniorData()
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
//        print(searchText)
        if searchText.isEmpty {
            filteredName = arrayOfJuniorExp
        }
        else{
            filteredName = arrayOfJuniorExp.filter({exp -> Bool in
                exp.JuniorExplorerName.lowercased().contains(searchText.lowercased())
            })
        }
        JunExpListTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        
        switch selectedScope {
        case 0:
            filteredName = arrayOfJuniorExp
        case 1:
            filteredName = arrayOfJuniorExp.filter({ exp -> Bool in
                exp.JuniorShift == "Morning"
            })
        case 2:
            filteredName = arrayOfJuniorExp.filter({ exp -> Bool in
                exp.JuniorShift == "Afternoon"
            })
        default:
            break
        }
        JunExpListTableView.reloadData()
    }
}

extension JunExpListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredName.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let JunExplorer = filteredName[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JunExpCell") as? JunExpListTableViewCell

        cell?.setJunExpList(junExplorer: JunExplorer)

        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(identifier: "DetailSenior") as? SenExpDetailViewController
        vc?.data2 = filteredName[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}
