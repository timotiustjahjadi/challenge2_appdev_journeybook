//
//  SenExpListViewController.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 14/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class SenExpListViewController: UIViewController {

    @IBOutlet weak var SenExpListTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        SenExpListTableView.delegate = self
        SenExpListTableView.dataSource = self

    }

    let arrayOfSeniorExp:[SenExplorer] = SenExplorer.fetchSeniorData()

}

extension SenExpListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfSeniorExp.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let SenExplorer = arrayOfSeniorExp[indexPath.row]

        let cell = tableView.dequeueReusableCell(withIdentifier: "SenExpCell") as? SenExpListTableViewCell

        cell?.setSenExpList(senExplorer: SenExplorer)

        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "DetailSenior") as? SenExpDetailViewController
        vc?.data = arrayOfSeniorExp[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)

    }

}
