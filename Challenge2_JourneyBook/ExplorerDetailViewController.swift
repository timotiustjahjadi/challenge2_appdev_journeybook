//
//  ExplorerDetailViewController.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 12/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class ExplorerDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

}
