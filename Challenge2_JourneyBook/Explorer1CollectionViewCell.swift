//
//  Explorer1CollectionViewCell.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 12/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class Explorer1CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var Explorer1ImageView: UIImageView!
    @IBOutlet weak var Explorer1NameLabel: UILabel!
    @IBOutlet weak var Explorer1RoleLabel: UILabel!
    
    func setExplorer1(junExplorer: JunExplorer){
        Explorer1ImageView.image = UIImage(named: junExplorer.JuniorExplorerImage)
        Explorer1NameLabel.text = junExplorer.JuniorExplorerName
        Explorer1RoleLabel.text = junExplorer.JuniorExplorerRole.uppercased()
    }
}
