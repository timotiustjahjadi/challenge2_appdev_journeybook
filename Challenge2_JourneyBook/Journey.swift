//
//  Journey.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 10/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import Foundation
import UIKit

struct Journey {
    var journeyImage:String
    var journeyTitle:String
    var journeyDur:String
    var learnGoal:String
    var constrains:String
    var group:Bool
    
    static func fetchDataJourney() -> [Journey]{
        var arrayOfJourneys: [Journey]
        
        arrayOfJourneys = [
            Journey(journeyImage: "challenge1", journeyTitle: "Challenge 1", journeyDur: "5", learnGoal: "Full Cycle CBL, Human Interface Guidelines, Prototyping, Xcode & Basic Swift Group Collaboration", constrains: "Not A Game, Not Back Ends, No Augmented Reality", group: true),
            Journey(journeyImage: "challenge2", journeyTitle: "Challenge 2", journeyDur: "2", learnGoal: "Applied Investigation, Everyone can code", constrains: "This is individual work, No third-party library, Use HIG interfaces essentials", group: false),
            Journey(journeyImage: "challenge3", journeyTitle: "Challenge 3", journeyDur: "4", learnGoal: "Collaboration, Best Practice Coding, Understanding User, Best Practice Project Management", constrains: "No Constraint", group: true),
            Journey(journeyImage: "challenge4", journeyTitle: "Challenge 4", journeyDur: "4", learnGoal: "TBA", constrains: "TBA", group: true),
            Journey(journeyImage: "challenge5", journeyTitle: "Challenge 5", journeyDur: "2", learnGoal: "TBA", constrains: "TBA", group: false),
            Journey(journeyImage: "challenge6", journeyTitle: "Macro Challenge", journeyDur: "12", learnGoal: "TBA", constrains: "TBA", group: true)
        ]
        
        return arrayOfJourneys
    }
}
