//
//  SenExpDetailViewController.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 14/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class SenExpDetailViewController: UIViewController {

    @IBAction func BackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var HelpLabel: UILabel!
    @IBOutlet weak var Help2Label: UILabel!
    
    @IBOutlet weak var SenExpImageView: UIImageView!
    @IBOutlet weak var SenExpNameLabel: UILabel!
    @IBOutlet weak var SenExpRoleLabel: UILabel!
    @IBOutlet weak var SenExpCanLabel: UILabel!
    @IBOutlet weak var SenExpNeedLabel: UILabel!
    @IBOutlet weak var ShiftLabel: UILabel!
    @IBOutlet weak var CallButtonOutlet: UIButton!
    @IBOutlet weak var MailButtonOutlet: UIButton!
    
    @IBAction func CallButton(_ sender: Any) {
        makeACall()
    }
    @IBAction func MailButton(_ sender: Any) {
        sendAMail()
    }
    
    var data2: JunExplorer = JunExplorer(JuniorExplorerImage: "", JuniorExplorerName: "", JuniorExplorerRole: "", JuniorCanHelp: "", JuniorNeedHelp: "", JuniorShift: "")
    var data: SenExplorer = SenExplorer(SeniorExplorerImage: "", SeniorExplorerName: "", SeniorExplorerRole: "", SeniorCanHelp: "", SeniorNeedHelp: "", SeniorPhone: "", SeniorEmail: "")
    
    var number:String = ""
    func makeACall() {
         if let url = URL(string: "tel://\(number)"),
         UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
              UIApplication.shared.open(url, options: [:], completionHandler:nil)
             } else {
                 UIApplication.shared.openURL(url)
             }
         } else {
                  // add error message here
         }
    }
    var email:String = ""
    func sendAMail(){
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HelpLabel.font = UIFont.boldSystemFont(ofSize: 16)
        Help2Label.font = UIFont.boldSystemFont(ofSize: 16)
        SenExpNameLabel.font = UIFont.boldSystemFont(ofSize: 34)
        if data2.JuniorExplorerName == ""{
            SenExpImageView.image = UIImage(named: data.SeniorExplorerImage)
            SenExpNameLabel.text = data.SeniorExplorerName

            SenExpRoleLabel.text = data.SeniorExplorerRole

            SenExpCanLabel.text = data.SeniorCanHelp

            SenExpNeedLabel.text = data.SeniorNeedHelp
            
            number = data.SeniorPhone
            email = data.SeniorEmail
            ShiftLabel.text = ""
        }
        else if(data.SeniorExplorerName == ""){
            SenExpImageView.image = UIImage(named: data2.JuniorExplorerImage)

            SenExpNameLabel.text = data2.JuniorExplorerName

            SenExpRoleLabel.text = data2.JuniorExplorerRole

            SenExpCanLabel.text = data2.JuniorCanHelp

            SenExpNeedLabel.text = data2.JuniorNeedHelp
            
            ShiftLabel.text = " | " + data2.JuniorShift
            
            CallButtonOutlet.isHidden = true
            MailButtonOutlet.isHidden = true

        }
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
