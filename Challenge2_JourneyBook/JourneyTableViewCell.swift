//
//  JourneyTableViewCell.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 10/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class JourneyTableViewCell: UITableViewCell {

    @IBOutlet weak var ChallengeImageView: UIImageView!
    @IBOutlet weak var ChallengeTitleLabel: UILabel!
    @IBOutlet weak var DurationLabel: UILabel!
    
    func setJourneys(journey: Journey){
        ChallengeImageView.image = UIImage(named: journey.journeyImage)
        ChallengeTitleLabel.text = journey.journeyTitle
        ChallengeTitleLabel.font = UIFont.boldSystemFont(ofSize: 22)
        DurationLabel.text = journey.journeyDur
    }
}
