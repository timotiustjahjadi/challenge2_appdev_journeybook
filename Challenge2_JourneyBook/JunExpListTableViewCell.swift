//
//  JunExpListTableViewCell.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 12/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class JunExpListTableViewCell: UITableViewCell {

    @IBOutlet weak var JunExpImageView: UIImageView!
    @IBOutlet weak var JunExpNameLabel: UILabel!
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
    func setJunExpList(junExplorer: JunExplorer){
        JunExpImageView.image = UIImage(named: junExplorer.JuniorExplorerImage)
        JunExpNameLabel.text = junExplorer.JuniorExplorerName
    }

}
