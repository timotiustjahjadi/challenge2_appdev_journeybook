//
//  JourneyDetailViewController.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 10/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class JourneyDetailViewController: UIViewController {

    @IBAction func BackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var ConstLabel: UILabel!
    @IBOutlet weak var LearnLabel: UILabel!
    
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var ChallengeImageView: UIImageView!
    
    @IBOutlet weak var GroupIndiImageView: UIImageView!
    @IBOutlet weak var GroupIndiLabel: UILabel!
    
    @IBOutlet weak var DurationLabel: UILabel!
    
    @IBOutlet weak var LearnGoals: UILabel!
    @IBOutlet weak var Constrains: UILabel!
    
    var data: Journey = Journey(journeyImage: "", journeyTitle: "", journeyDur: "", learnGoal: "", constrains: "", group: true)

    override func viewDidLoad() {
        super.viewDidLoad()
        ConstLabel.font = UIFont.boldSystemFont(ofSize: 16)
        LearnLabel.font = UIFont.boldSystemFont(ofSize: 16)
        TitleLabel.font = UIFont.boldSystemFont(ofSize: 34)
        ChallengeImageView.image = UIImage(named: data.journeyImage)
        
        TitleLabel.text = data.journeyTitle
        
        if data.group == true {
            GroupIndiImageView.image = UIImage(systemName: "person.3.fill")
            GroupIndiLabel.text = "Group Challenge"
        }
        else{
            GroupIndiImageView.image = UIImage(systemName: "person.fill")
            GroupIndiLabel.text = "Individual Challenge"
        }
        
        DurationLabel.text = data.journeyDur
        
        LearnGoals.text = data.learnGoal
        
        Constrains.text = data.constrains
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
}
