//
//  ExplorerListViewController.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 11/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class ExplorerListViewController: UIViewController{

   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SeniorExpCollection.delegate = self
        SeniorExpCollection.dataSource = self

        JuniorExpCollection.delegate = self
        JuniorExpCollection.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var SeniorExpCollection: UICollectionView!
    @IBOutlet weak var JuniorExpCollection: UICollectionView!
    
    var arrayOfJuniorExp:[JunExplorer] = JunExplorer.fetchJuniorData()
    var arrayOfSeniorExp:[SenExplorer] = SenExplorer.fetchSeniorData()

}

extension ExplorerListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = SeniorExpCollection.dequeueReusableCell(withReuseIdentifier: "SenExplorerCell", for: indexPath) as? ExplorerCollectionViewCell
        if collectionView == JuniorExpCollection{
            let cell2 = JuniorExpCollection.dequeueReusableCell(withReuseIdentifier: "JunExplorerCell", for: indexPath) as? Explorer1CollectionViewCell

            let exp2 = arrayOfJuniorExp[indexPath.row]

            cell2?.setExplorer1(junExplorer: exp2)

            return cell2!
        }

        let exp = arrayOfSeniorExp[indexPath.row]
        
        cell?.setExplorer(senExplorer: exp)

        return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == JuniorExpCollection{
            let vc = storyboard?.instantiateViewController(identifier: "DetailSenior") as? SenExpDetailViewController
            vc?.data2 = arrayOfJuniorExp[indexPath.row]
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else{
            let vc = storyboard?.instantiateViewController(identifier: "DetailSenior") as? SenExpDetailViewController
            vc?.data = arrayOfSeniorExp[indexPath.row]
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }

}
