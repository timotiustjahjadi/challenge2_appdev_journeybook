//
//  SenExpListTableViewCell.swift
//  Challenge2_JourneyBook
//
//  Created by Timotius Tjahjadi  on 14/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import UIKit

class SenExpListTableViewCell: UITableViewCell {

    @IBOutlet weak var SenExpImageView: UIImageView!
    @IBOutlet weak var SenExpNameLabel: UILabel!

//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

    func setSenExpList(senExplorer: SenExplorer){
        SenExpImageView.image = UIImage(named: senExplorer.SeniorExplorerImage)
        SenExpNameLabel.text = senExplorer.SeniorExplorerName
    }

}
